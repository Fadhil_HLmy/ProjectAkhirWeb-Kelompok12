<?php
include_once 'top.php';
require_once 'db/class_pkm.php';
?>
<legend><h2>Kegiatan PKM</h2></legend>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <ul style="background-color: white;" class="breadcrumb">
                <li >
                    <a href="/Project_dbKampus/index.php">Menu</a><span class="divider"></span>
                </li>
                <li class="active">Kegiatan PKM</li>
            </ul>
        </div>
    </div>
</div>
<div class="panel-header">
    <a class="btn icon-btn btn-success" href="form_pkm.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Kegiatan 
    </a>
</div>
<br>
<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script><br>
<body>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr style="color: white; background-color: black">
        <th>No</th>
        <th>Nama</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody style="color: #007EA7">
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td><a href="view_pkm.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_pkm.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
</body><br><br>
<?php
include_once 'bottom.php';
?>