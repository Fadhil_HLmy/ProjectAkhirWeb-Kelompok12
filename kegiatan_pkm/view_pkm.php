<?php
    include_once 'top.php';
    require_once 'db/class_pkm.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objKegiatan = new Kegiatan();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Kegiatan PKM</h3>
            </div><br>
            <div class="container-fluid">
                <div class="row">
                     <div class="col-md-12">
                         <ul style="background-color: #242326;" class="breadcrumb">
                            <li >
                                <a href="kegiatan_pkm.php">Kegiatan PKM</a><span class="divider"></span>
                            </li>
                            <li class="active">View PKM</li>
                         </ul>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">ID</td><td>:</td><td><?php echo
                $data['id']?></td>
                </tr>
                <tr>
                <td class="active">Nama</td><td>:</td><td><?php echo
                $data['nama']?></td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom_2.php';
?>