<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_pkm.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new Kegiatan();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_kegiatan.js"></script>
<form name="form_pkm" class="form-horizontal" method="POST" action="proses_pkm.php">
<fieldset>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <ul style="background-color: #242326;" class="breadcrumb">
                <li >
                    <a href="kegiatan_pkm.php">Kegiatan PKM</a><span class="divider"></span>
                </li>
                <li class="active">Form PKM</li>
            </ul>
        </div>
    </div>
</div>

<!-- Form Name -->
<legend>Form Kegiatan PKM</legend>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama</label>  
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Masukkan Nama" class="form-control input-md" value="<?php echo $data['nama']?>" >
    
  </div>
</div>


<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom_2.php';
?>