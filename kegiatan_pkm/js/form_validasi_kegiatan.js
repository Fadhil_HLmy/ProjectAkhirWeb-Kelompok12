//buat fungsi
$(function(){
    //panggil nama form lalu gunakan fungsi validasi
    $("form[name='form_pkm']").validate({

        //buat aturan untuk input form
        rules:{
            kode:{
                required:true,
                maxLength:45,
            },
            judul:"required",
            narasumber:"required",
            deskripsi:"required",
        },
        //tampilkan pesan
        messages:{
            kode:{
                required:"Data tidak ada, Mohon mengisi data terlebih dahulu !",
                maxLength:"Max 45 Character",
            },
            judul:"Nama wajib diisi!!",
            narasumber:"Nama wajib diisi",
            deskripsi:"Deskripsi wajib diisi",
        },
        //submit form
        submitHandler:function(form){
            form.submit();
        }
    });

});