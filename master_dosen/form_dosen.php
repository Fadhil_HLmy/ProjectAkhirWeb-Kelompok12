<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_pkm.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new Kegiatan();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_kegiatan.js"></script>
<form name="form_kegiatan" class="form-horizontal" method="POST" action="proses_dosen.php">
<fieldset>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <ul style="background-color: #242326;" class="breadcrumb">
        <li >
          <a href="master_dosen.php">Master Dosen</a><span class="divider"></span>
        </li>
        <li class="active">Form Master Dosen</li>
      </ul>
    </div>
  </div>
</div>

<!-- Form Name -->
<legend>Form Master Dosen</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="id">ID</label>  
  <div class="col-md-4">
  <input id="id" name="id" type="text" placeholder="Masukkan id" class="form-control input-md" value="<?php echo $data['id']?>">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nidn">NIDN</label>  
  <div class="col-md-4">
  <input id="nidn" name="nidn" type="text" placeholder="Masukkan NIDN" class="form-control input-md" value="<?php echo $data['nidn']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama</label>  
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Masukkan Nama" class="form-control input-md" value="<?php echo $data['nama']?>">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="gelar_depan">Gelar Depan</label>  
  <div class="col-md-4">
  <input id="gelar_depan" name="gelar_depan" type="text" placeholder="Masukkan Gelar Depan" class="form-control input-md" value="<?php echo $data['gelar_depan']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="gelar_belakang">Gelar Belakang</label>  
  <div class="col-md-4">
  <input id="gelar_belakang" name="gelar_belakang" type="text" placeholder="Masukkan Gelar Belakang" class="form-control input-md" value="<?php echo $data['gelar_belakang']?>">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tmp_lahir">Tempat Lahir</label>  
  <div class="col-md-4">
  <input id="tmp_lahir" name="tmp_lahir" type="text" placeholder="Masukkan Tempat Lahir" class="form-control input-md" value="<?php echo $data['tmp_lahir']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tgl_lahir">Tanggal Lahir</label>  
  <div class="col-md-4">
  <input id="tgl_lahir" name="tgl_lahir" type="text" placeholder="Masukkan Tanggal Lahir" class="form-control input-md" value="<?php echo $data['tgl_lahir']?>">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="jk">Jenis Kelamin</label>  
  <div class="col-md-4">
  <input id="jk" name="jk" type="text" placeholder="Masukkan Jenis Kelamin" class="form-control input-md" value="<?php echo $data['jk']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="prodi_id">Prodi ID</label>  
  <div class="col-md-4">
  <input id="prodi_id" name="prodi_id" type="text" placeholder="Masukkan ID Prodgram Didik" class="form-control input-md" value="<?php echo $data['prodi_id']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">E-mail</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="Masukkan E-mail" class="form-control input-md" value="<?php echo $data['email']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="jabatan_id">ID Jabatan</label>  
  <div class="col-md-4">
  <input id="jabatan_id" name="jabatan_id" type="text" placeholder="Masukkan ID Jabatan" class="form-control input-md" value="<?php echo $data['jabatan_id']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pend_akhir">Pendidikan Akhir</label>  
  <div class="col-md-4">
  <input id="pend_akhir" name="pend_akhir" type="text" placeholder="Masukkan Pendidikan Akhir" class="form-control input-md" value="<?php echo $data['pend_akhir']?>" >
    
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>