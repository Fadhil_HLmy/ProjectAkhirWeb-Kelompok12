<?php
    include_once 'top.php';
    require_once 'db/class_pkm.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objKegiatan = new Kegiatan();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Master Dosen</h3>
            </div><br>
            <div class="container-fluid">
                <div class="row">
                     <div class="col-md-12">
                         <ul style="background-color: #242326;" class="breadcrumb">
                            <li >
                                <a href="master_dosen.php">Master Dosen</a><span class="divider"></span>
                            </li>
                            <li class="active">View Dosen</li>
                         </ul>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">ID</td><td>:</td><td><?php echo
                $data['id']?></td>
                </tr>
                <tr>
                <td class="active">NIDN</td><td>:</td><td><?php echo
                $data['nidn']?></td>
                </tr>
                 <td class="active">Nama</td><td>:</td><td><?php echo
                $data['nama']?></td>
                </tr>
                <tr>
                <td class="active">Gelar Depan</td><td>:</td><td><?php echo
                $data['gelar_depan']?></td>
                </tr>
                 <td class="active">Gelar Belakang</td><td>:</td><td><?php echo
                $data['gelar_belakang']?></td>
                </tr>
                <tr>
                <td class="active">Tempat Lahir</td><td>:</td><td><?php echo
                $data['tmp_lahir']?></td>
                </tr>
                <td class="active">Tanggal Lahir</td><td>:</td><td><?php echo
                $data['tgl_lahir']?></td>
                </tr>
                <tr>
                <td class="active">Jenis Kelamin</td><td>:</td><td><?php echo
                $data['jk']?></td>
                </tr>
                <tr>
                <td class="active">ID Prodi</td><td>:</td><td><?php echo
                $data['prodi_id']?></td>
                </tr>
                <td class="active">E-mail</td><td>:</td><td><?php echo
                $data['email']?></td>
                </tr>
                <tr>
                <td class="active">ID Jabatan</td><td>:</td><td><?php echo
                $data['jabatan_id']?></td>
                </tr>
                <tr>
                <td class="active">Pendidikan Akhir</td><td>:</td><td><?php echo
                $data['pend_akhir']?></td>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>