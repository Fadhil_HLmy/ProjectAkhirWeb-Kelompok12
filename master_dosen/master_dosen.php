<?php
include_once 'top.php';
require_once 'db/class_pkm.php';
?>
<legend><h2>Master Dosen</h2></legend>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <ul style="background-color: white;" class="breadcrumb">
                <li >
                    <a href="/Project_dbKampus/index.php">Menu</a><span class="divider"></span>
                </li>
                <li class="active">Master Dosen</li>
            </ul>
        </div>
    </div>
</div>

<div align="left" class="panel-header">
    <a class="btn icon-btn btn-success" href="form_dosen.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Kegiatan 
    </a>
</div>
<br>
<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr style="color: white; background-color: black">
        <th>No</th>
        <th>NIDN</th>
        <th>Nama</th>
        <th>Gelar Depan</th>
        <th>Gelar Belakang</th>
        <th>Tempat Lahir</th>
        <th>Tanggal Lahir</th>
        <th>Jenis Kelamin</th>
        <th>ID Prodi</th>
        <th>Email</th>
        <th>ID Jabatan</th>
        <th>Pendidikan Akhir</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody style="color: #007EA7">
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nidn'].'</td>';
        echo '<td>'.$row['nama'].'</td>';
        echo '<td>'.$row['gelar_depan'].'</td>';
        echo '<td>'.$row['gelar_belakang'].'</td>';
        echo '<td>'.$row['tmp_lahir'].'</td>';
        echo '<td>'.$row['tgl_lahir'].'</td>';
        echo '<td>'.$row['jk'].'</td>';
        echo '<td>'.$row['prodi_id'].'</td>';
        echo '<td>'.$row['email'].'</td>';
        echo '<td>'.$row['jabatan_id'].'</td>';
        echo '<td>'.$row['pend_akhir'].'</td>';
        echo '<td><a href="view_dosen.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_dosen.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table><br><br>
<a href="grafik_kegiatan.php"><button style="width: 100px" type="button" class="btn btn-info">INFO</button></a>
<h5 style="color: red">*Lihat Selengkapnya</h5>
<?php
include_once 'bottom.php';
?>