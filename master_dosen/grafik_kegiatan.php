<?php
    include_once 'top.php';
    require_once 'db/class_pkm.php';
    $obj = new Kegiatan();

    $rs = $obj ->getStatistik();
    $ar_data = [];
    foreach ($rs as $row) {
        $ar['label'] = $row['nama'];
        $ar['label'] = $row['judul'];
        $ar['y']=(int)$row['biaya'];
        $ar_data[]=$ar;
    }
$out = array_values($ar_data);
?>
<legend><h2>Statistik Cost Seminar</h2></legend>
<!--buat javascript untuk menampilkan data-->
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <ul style="background-color: #242326;" class="breadcrumb">
        <li >
          <a href="master_dosen.php">Master Dosen</a><span class="divider"></span>
        </li>
        <li class="active">Chart Master Dosen</li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript">
    window.onload = function(){
        var chart = new CanvasJS.Chart ("chartContainer",{
            theme: "light1",
            animationEnabled: true,
            tittle:{
                    text: "Basic Column Chart"
                },
            data: [ {
                type: "column",
                dataPoints: <?php echo json_encode($out) ?>
            }]
        });
        chart.render();
    }
</script>
<body>
    <div id="chartContainer" style="height: 370px; width: 100%;"></div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js">
    </script>
</body>
<!--Tempat menampilkan chart-->
<!--panggil file jquery untuk grafik-->
<?php
    include_once 'bottom_2.php';
?>