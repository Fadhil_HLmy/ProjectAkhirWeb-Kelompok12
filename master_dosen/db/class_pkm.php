<?php
    /*
mysql> select* from dosen;
+----+------------+------------------------+-------------+----------------+-----------+-----------+------+----------+-------+------------+------------+
| id | nidn       | nama                   | gelar_depan | gelar_belakang | tmp_lahir | tgl_lahir | jk   | prodi_id | email | jabatan_id | pend_akhir |
+----+------------+------------------------+-------------+----------------+-----------+-----------+------+----------+-------+------------+------------+
|  1 | 0413128601 | AHMAD RIO ADRIANSYAH   | NULL        | S.Si,M.Si      | NULL      | NULL      | L    |        2 | NULL  |          2 | S2         |
|  2 | 0402018701 | AMALIA RAHMAH          | NULL        | S.T,M.T        | NULL      | NULL      | P    |        1 | NULL  |          2 | S2         |
|  3 | 0404017602 | BACHTIAR FIRDAUS       | NULL        | S.T,M.P        | NULL      | NULL      | L    |        1 | NULL  |          1 | S2         |
|  4 | 9903019258 | BAMBANG PRIANTONO      | NULL        | M.T,Dr         | NULL      | NULL      | L    |        1 | NULL  |          1 | S3         |
|  5 | 0407097405 | HENRY SAPTONO          | NULL        | S.Si,M.Kom     | NULL      | NULL      | L    |        2 | NULL  |          2 | S2         |
|  6 | 0411118402 | HILMY ABIDZAR TAWAKAL  | NULL        | S.T,M.Kom      | NULL      | NULL      | L    |        2 | NULL  |          2 | S2         |
|  7 | 0413038701 | INDRA HERMAWAN         | NULL        | S.Kom,M.Kom    | NULL      | NULL      | L    |        2 | NULL  |          2 | S2         |
|  8 | 0415108403 | KURNIAWAN DWI PRASETYO | NULL        | S.T,M.T        | NULL      | NULL      | L    |        1 | NULL  |          1 | S2         |
|  9 | 0421117805 | LUKMAN ROSYIDI         | NULL        | S.T,M.M.,M.T   | NULL      | NULL      | L    |        2 | NULL  |          2 | S2         |
| 10 | 0420097706 | MUHAMMAD BINTANG       | NULL        | S.Kom          | NULL      | NULL      | L    |        1 | NULL  |          1 | S2         |
| 11 | 0431088701 | NUGROHO DWI SAPUTRA    | NULL        | S.Kom,M.Ti     | NULL      | NULL      | L    |        1 | NULL  |          1 | S2         |
| 12 | 0429058305 | REZA ALDIANSYAH        | NULL        | S.T,M.Ti       | NULL      | NULL      | L    |        1 | NULL  |          1 | S2         |
| 13 | 0427057704 | REZA PRIMARDIANSYAH    | NULL        | S.T,M.Kom      | NULL      | NULL      | L    |        2 | NULL  |          1 | S2         |
| 14 | 0423076303 | RUSMANTO               | NULL        | M.M.,Drs       | NULL      | NULL      | L    |        1 | NULL  |          2 | S2         |
| 15 | 0424088901 | SALMAN EL FARISI       | NULL        | S.Kom,M.Kom    | NULL      | NULL      | L    |        2 | NULL  |          1 | S2         |
| 16 | 0426096501 | SAPTO WALUYO           | NULL        | S.Sos,M.Sc.    | NULL      | NULL      | L    |        1 | NULL  |          1 | S2         |
| 17 | 0414047101 | SIROJUL MUNIR          | NULL        | S.Si,M.Kom     | NULL      | NULL      | L    |        2 | NULL  |          2 | S2         |
| 18 | 0006067204 | SUHENDI                | NULL        | S.T,M.M.S.I    | NULL      | NULL      | L    |        1 | NULL  |          2 | S2         |
| 19 | 0401017122 | WARSONO                | NULL        | S.Kom,M.Ti     | NULL      | NULL      | L    |        1 | NULL  |          1 | S2         |
| 20 | 0426088302 | ZAKI IMADUDDIN         | NULL        | S.T,M.Kom      | NULL      | NULL      | L    |        2 | NULL  |          2 | S2         |
+----+------------+------------------------+-------------+----------------+-----------+-----------+------+----------+-------+------------+------------+
20 rows in set (0,00 sec)


    */
    require_once "DAO_pkm.php";
    class Kegiatan extends DAO_pkm
    {
        public function __construct()
        {
            parent::__construct("dosen");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,nidn,nama,gelar_depan,gelar_belakang,tmp_lahir,tgl_lahir, jk, prodi_id, email, jabatan_id, pend_akhir) ".
            " VALUES (default,?,?,?,?,?,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nidn = ?, nama = ?, gelar_depan = ?, gelar_belakang = ?, tmp_lahir = ?, tgl_lahir = ?, jk = ?, prodi_id = ?, email = ?, jabatan_id = ?, pend_akhir = ?".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik
        public function getStatistik(){
        $sql = "SELECT dosen.nama,pkm_dosen.judul,pkm_dosen.biaya from dosen
        INNER JOIN  pkm_dosen ON dosen.id = pkm_dosen.dosen_id" ;
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
        }
    }
?>
