<?php
include_once 'top.php';

?>
<body style="background-color: #00171F;">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="carousel slide" id="carousel-43812">
				<ol class="carousel-indicators">
					<li class="active" data-slide-to="1000" data-target="#carousel-43812">
					</li>
					<li data-slide-to="1" data-target="#carousel-43812">
					</li>
					<li data-slide-to="2" data-target="#carousel-43812">
					</li>
				</ol>
				<div style="width: auto; height: auto;" class="carousel-inner">
					<div align="center" class="item active">
						<img style="height: 500px" alt="Carousel Bootstrap First" src="https://i.pinimg.com/originals/40/1e/6c/401e6ce81c674e75a47683ed4d8c4db6.jpg">
						<div class="carousel-caption">
						<h4 class="tentang">STT Nurul Fikri</h4>
							<p class="tentang">#Technopreneur Campus</p>
						</div>
					</div>
					<div align="center" class="item">
						<img style="height: 500px" alt="Carousel Bootstrap Second" src="/Project_dbKampus/935-proposal.png">
						<div class="carousel-caption">
							<h4 class="tentang">Program Kreative Mahasiswa</h4>
							<p class="tentang">#GO PIMNAS 2018</p>
						</div>
					</div>
					<div align="center" class="item">
						<img style="height: 500px" alt="Carousel Bootstrap Third" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Logo_Kemenristekdikti.png/643px-Logo_Kemenristekdikti.png">
						<div class="carousel-caption">
							<h4 class="tentang">Kementrian Riset Dan Teknologi</h4>
							<p class="tentang">#Melayani dan Mengayomi</p>
						</div>
					</div>
				</div> 
				<a class="left carousel-control" href="#carousel-43812" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span></a> 
				<a class="right carousel-control" href="#carousel-43812" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
		</div>
	</div>
</div>
</body>
<br>
<?php
include_once 'bottom.php';
?>