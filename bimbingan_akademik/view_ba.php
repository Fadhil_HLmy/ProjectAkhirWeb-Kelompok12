<?php
    include_once 'top.php';
    require_once 'db/class_pkm.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objKegiatan = new Kegiatan();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Bimbingan Akademik</h3>
            </div><br>
            <div class="container-fluid">
                <div class="row">
                     <div class="col-md-12">
                         <ul style="background-color: #242326;" class="breadcrumb">
                            <li >
                                <a href="bimbingan_akademik.php">Bimbingan Akademik</a><span class="divider"></span>
                            </li>
                            <li class="active">View Bimbingan Akademik</li>
                         </ul>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table">
                <tr>
                <td class="active">ID</td><td>:</td><td><?php echo
                $data['id']?></td>
                </tr>
                <tr>
                <td class="active">Tanggal</td><td>:</td><td><?php echo
                $data['tanggal']?></td>
                </tr>
                 <td class="active">Bimbingan Presensi</td><td>:</td><td><?php echo
                $data['bimbingan_presensi']?></td>
                </tr>
                <tr>
                <td class="active">Bimbingan Keuangan</td><td>:</td><td><?php echo
                $data['bimbingan_keuangan']?></td>
                </tr>
                 <td class="active">Bimbingan Akademik</td><td>:</td><td><?php echo
                $data['bimbingan_akademik']?></td>
                </tr>
                <tr>
                <td class="active">ID Kategori</td><td>:</td><td><?php echo
                $data['kategori_id']?></td>
                </tr>
                <td class="active">Semester</td><td>:</td><td><?php echo
                $data['semester']?></td>
                </tr>
                <tr>
                <td class="active">NIM</td><td>:</td><td><?php echo
                $data['nim']?></td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>