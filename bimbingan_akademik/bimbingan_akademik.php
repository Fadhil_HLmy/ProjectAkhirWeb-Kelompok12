<?php
include_once 'top.php';
require_once 'db/class_pkm.php';
?>
<legend><h2>Bimbingan Akademik</h2></legend>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <ul style="background-color: white;" class="breadcrumb">
                <li >
                    <a href="/Project_dbKampus/index.php">Menu</a><span class="divider"></span>
                </li>
                <li class="active">Bimbingan Akademik</li>
            </ul>
        </div>
    </div>
</div>

<div align="left" class="panel-header">
    <a class="btn icon-btn btn-success" href="form_ba.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Kegiatan 
    </a>
</div>
<br>
<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr style="color: white; background-color: black">
        <th>No</th>
        <th>Tanggal</th>
        <th>Bimbingan Presensi</th>
        <th>Bimbingan Keuangan</th>
        <th>Bimbingan Akademik</th>
        <th>ID Kategori</th>
        <th>Semester</th>
        <th>NIM</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody style="color: #007EA7">
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['tanggal'].'</td>';
        echo '<td>'.$row['bimbingan_presensi'].'</td>';
        echo '<td>'.$row['bimbingan_keuangan'].'</td>';
        echo '<td>'.$row['bimbingan_akademik'].'</td>';
        echo '<td>'.$row['kategori_id'].'</td>';
        echo '<td>'.$row['semester'].'</td>';
        echo '<td>'.$row['nim'].'</td>';
        echo '<td><a href="view_ba.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_ba.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
include_once 'bottom.php';
?>