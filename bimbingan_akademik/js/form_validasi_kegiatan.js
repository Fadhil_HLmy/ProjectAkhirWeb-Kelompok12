//buat fungsi
$(function(){
    //panggil nama form lalu gunakan fungsi validasi
    $("form[name='form_kegiatan']").validate({

        //buat aturan untuk input form
        rules:{
            kode:{
                required:true,
                maxLength:10,
            },
            judul:"required",
            narasumber:"required",
            deskripsi:"required",
        },
        //tampilkan pesan
        messages:{
            kode:{
                required:"Kode Wajib diisi!!",
                maxLength:"Max 10 Character",
            },
            judul:"Judul wajib diisi!!",
            narasumber:"Narasumber wajib diisi",
            deskripsi:"Deskripsi wajib diisi",
        },
        //submit form
        submitHandler:function(form){
            form.submit();
        }
    });

});


