<?php
    /*
mysql> select *from bimbingan_akademik;
+----+------------+------------------------------------+--------------------+-----------------------------+-------------+----------+-----------+
| id | tanggal    | bimbingan_presensi                 | bimbingan_keuangan | bimbingan_akademik          | kategori_id | semester | nim       |
+----+------------+------------------------------------+--------------------+-----------------------------+-------------+----------+-----------+
|  1 | 2017-12-20 | Kehadiran PBO kurang               | ok                 | ok                          |           3 |    20171 | 110116059 |
|  2 | 2017-12-20 | Kehadiran BASDAT dan METPEN Kurang | -                  | Nilai UTS masih belum bagus |           3 |    20171 | 110216018 |
+----+------------+------------------------------------+--------------------+-----------------------------+-------------+----------+-----------+
2 rows in set (0,00 sec)



    */
    require_once "DAO_pkm.php";
    class Kegiatan extends DAO_pkm
    {
        public function __construct()
        {
            parent::__construct("bimbingan_akademik");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id, tanggal, bimbingan_presensi, bimbingan_keuangan, bimbingan_akademik, kategori_id, semester, nim) ".
            " VALUES (default,?,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET tanggal = ?, bimbingan_presensi = ?, bimbingan_keuangan = ?, bimbingan_akademik = ?, kategori_id = ?, semester = ?, nim = ?".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik
    }
?>