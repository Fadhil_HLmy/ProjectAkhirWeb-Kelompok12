<?php
    /*
mysql> select* from pkm_dosen;
+----+---------------+---------------+-------------------------------+-------------------------+--------+----------+----------+-----------------+
| id | tanggal_mulai | tanggal_akhir | judul                         | tempat                  | biaya  | dosen_id | semester | kategori_pkm_id |
+----+---------------+---------------+-------------------------------+-------------------------+--------+----------+----------+-----------------+
|  1 | 2017-12-10    | 2017-12-10    | Training Web Desain           | Pesantren TIK Lazis PLN | 300000 |       18 |    20171 |               1 |
|  2 | 2017-12-15    | 2017-12-15    | Trainin Web Security          | Mabes TNI Cilangkap     | 350000 |        5 |    20171 |               1 |
|  3 | 2017-12-20    | 2017-12-20    | Pengembangan Aplikasi Android | SMK Amaliyah Jakarta    | 350000 |        6 |    20171 |               1 |
+----+---------------+---------------+-------------------------------+-------------------------+--------+----------+----------+-----------------+
3 rows in set (0,09 sec)


    */
    require_once "DAO_pkm.php";
    class Kegiatan extends DAO_pkm
    {
        public function __construct()
        {
            parent::__construct("pkm_dosen");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,tanggal_mulai,tanggal_akhir,judul,tempat,biaya,dosen_id,semester,kategori_pkm_id) ".
            " VALUES (default,?,?,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET tanggal_mulai = ?, tanggal_akhir = ?, judul = ?, tempat = ?, biaya = ?, dosen_id = ?, semester = ?, kategori_pkm_id = ? ".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik
        public function getStatistik(){
        $sql = "SELECT kategori_pkm.nama,COUNT(pkm_dosen.id) as biaya from pkm_dosen
        LEFT JOIN  kategori_pkm ON pkm_dosen.kategori_pkm_id = kategori_pkm.id GROUP BY kategori_pkm.nama" ;
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
    }
     public function getAlldosen(){
        $sql = "SELECT * FROM dosen ";
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
    }
}
?>
