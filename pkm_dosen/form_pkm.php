<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_pkm.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new Kegiatan();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_kegiatan.js"></script>
<form name="form_kegiatan" class="form-horizontal" method="POST" action="proses_pkm.php">
<fieldset>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <ul style="background-color: #242326;" class="breadcrumb">
        <li >
          <a href="pkm_dosen.php">Dosen PKM</a><span class="divider"></span>
        </li>
        <li class="active">Form PKM Dosen</li>
      </ul>
    </div>
  </div>
</div>

<!-- Form Name -->
<legend>Form Dosen PKM</legend>

<!--

<div class="form-group">
  <label class="col-md-4 control-label" for="id">ID</label>  
  <div class="col-md-4">
  <input id="id" name="id" type="text" placeholder="Masukkan id" class="form-control input-md" value="<?php echo $data['id']?>">
    
  </div>
</div>
-->

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tanggal_mulai">Tanggal Mulai</label>  
  <div class="col-md-4">
  <input id="tanggal_mulai" name="tanggal_mulai" type="text" placeholder="Masukkan Tanggal Mulai" class="form-control input-md" value="<?php echo $data['tanggal_mulai']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="id">Tanggal Akhir</label>  
  <div class="col-md-4">
  <input id="tanggal_akhir" name="tanggal_akhir" type="text" placeholder="Masukkan Tanggal Akhir" class="form-control input-md" value="<?php echo $data['tanggal_akhir']?>">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Judul</label>  
  <div class="col-md-4">
  <input id="judul" name="judul" type="text" placeholder="Masukkan Judul" class="form-control input-md" value="<?php echo $data['judul']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tempat">Tempat</label>  
  <div class="col-md-4">
  <input id="tempat" name="tempat" type="text" placeholder="Masukkan id" class="form-control input-md" value="<?php echo $data['tempat']?>">
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="biaya">Biaya</label>  
  <div class="col-md-4">
  <input id="biaya" name="biaya" type="text" placeholder="Masukkan Biaya" class="form-control input-md" value="<?php echo $data['biaya']?>" >
    
  </div>
</div>
<?php
$dosens = $obj_kegiatan->getAlldosen();

?>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="dosen_id">Dosen ID</label>  
  <div class="col-md-4">
  <select name = "dosen_id">
    <?php
    foreach ($dosens as $dosen) {
      echo '<option value = "'.$dosen['id'].'">'.$dosen['nama'].'</option>';
    }

    ?>

  </select>
    
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="semester">Semester</label>  
  <div class="col-md-4">
  <input id="semester" name="semester" type="text" placeholder="Masukkan Semester" class="form-control input-md" value="<?php echo $data['semester']?>" >
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">ID PKM</label>  
  <div class="col-md-4">
  <input id="kategori_pkm_id" name="kategori_pkm_id" type="text" placeholder="Masukkan ID PKM" class="form-control input-md" value="<?php echo $data['kategori_pkm_id']?>" >
    
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>