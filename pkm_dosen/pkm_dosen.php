<?php
include_once 'top.php';
require_once 'db/class_pkm.php';
?>
<legend><h2>Dosen PKM</h2></legend>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <ul style="background-color: white;" class="breadcrumb">
                <li >
                    <a href="index.php">Menu</a><span class="divider"></span>
                </li>
                <li class="active">Dosen PKM</li>
            </ul>
        </div>
    </div>
</div>

<div align="left" class="panel-header">
    <a class="btn icon-btn btn-success" href="form_pkm.php">
    <span class="glyphicon btn-glyphicon glyphicon-plus img-
    circle text-success"></span>
    Tambah Kegiatan 
    </a>
</div>
<br>
<?php
$obj = new Kegiatan();
$rows = $obj->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
<script languange="JavaScript">
    $(document).ready(function(){
        $('#example').DataTable();
    });
</script>
<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    <thead>
    <tr style="color: white; background-color: black">
        <th>No</th>
        <th>Tanggal Mulai</th>
        <th>Tanggal Akhir</th>
        <th>Judul</th>
        <th>Tempat</th>
        <th>Biaya</th>
        <th>ID_Dosen</th>
        <th>Semester</th>
        <th>Kategori PKM_ID</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody style="color: #007EA7">
    <?php
    $nomor = 1;
    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['tanggal_mulai'].'</td>';
        echo '<td>'.$row['tanggal_akhir'].'</td>';
        echo '<td>'.$row['judul'].'</td>';
        echo '<td>'.$row['tempat'].'</td>';
        echo '<td>'.$row['biaya'].'</td>';
        echo '<td>'.$row['dosen_id'].'</td>';
        echo '<td>'.$row['semester'].'</td>';
        echo '<td>'.$row['kategori_pkm_id'].'</td>';
        echo '<td><a href="view_pkm.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_pkm.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table><br><br>
<a href="grafik_kegiatan.php"><button style="width: 100px" type="button" class="btn btn-info">INFO</button></a>
<h5 style="color: red">*Lihat Selengkapnya</h5>
<?php
include_once 'bottom.php';
?>