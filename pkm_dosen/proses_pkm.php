<?php
    //panggil file yang berisi oeperasi db
    require_once 'db/class_pkm.php';
    //buat variabel untuk melakukan operasi db
    $obj = new Kegiatan();
    //buat variabel untuk mengambil data dari form dan menyimpannya dlm array
    $_tanggal_mulai = $_POST['tanggal_mulai'];
    $_tanggal_akhir = $_POST['tanggal_akhir'];
    $_judul = $_POST['judul'];
    $_tempat = $_POST['tempat'];
    $_biaya = $_POST['biaya'];
    $_dosen_id = $_POST['dosen_id'];
    $_semester = $_POST['semester'];
    $_kategori_pkm_id = $_POST['kategori_pkm_id'];

    $_proses = $_POST['proses'];

    $ar_data[] = $_tanggal_mulai;
    $ar_data[] = $_tanggal_akhir;
    $ar_data[] = $_judul;
    $ar_data[] = $_tempat;
    $ar_data[] = $_biaya;
    $ar_data[] = $_dosen_id;
    $ar_data[] = $_semester;
    $ar_data[] = $_kategori_pkm_id;    
    //buat operasi jika memilih button simpan, update atau hapus
    $row = 0;
    if($_proses == "Simpan"){
        $row = $obj->simpan($ar_data);
    }elseif($_proses == "Update"){
        $_idedit = $_POST['idedit'];
        $ar_data[] = $_idedit;
        $row = $obj->ubah($ar_data);
    }elseif($_proses == "Hapus"){
        unset($ar_data);
        $_idedit = $_POST['idedit'];
        $row = $obj->hapus($_idedit);
    }
    //handeler jika gagal atau sukses
    if($row==0){
        echo "Gagal Proses";
    }else{
        //echo "Proses Sukses";
        //langsung direct ke daftar_kegiatan.php
        header('Location:pkm_dosen.php');
    }
?>