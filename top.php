<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>STT Nurul - Fikri</title>
    <link href='http://arryrahmawan.net/wp-content/uploads/2014/06/STT-Terpadu-NF.png' rel='shortcut icon'>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

	<link href="kegiatan_pkm/css/bootstrap_united.min.css" rel="stylesheet">
	<link rel="stylesheet" href="kegiatan_pkm/css/font-awesome.min.css">
    <link href="kegiatan_pkm/css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="kegiatan_pkm/js/dataTables.bootstrap.min.js"/>


	<script src="kegiatan_pkm/js/jquery.min.js"></script>
	<script src="kegiatan_pkm/js/jquery.dataTables.min.js"></script>
    <script src="kegiatan_pkm/js/bootstrap.min.js"></script>
    <script src="kegiatan_pkm/js/scripts.js"></script>
	<script src="kegiatan_pkm/js/jquery.validate.js"></script>


  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav style="background-color: #242326" class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="index.php">NF - PKM</a>
				</div>
				
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">LIST PKM<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="kegiatan_pkm/kegiatan_pkm.php">Kegiatan PKM</a>
								</li>
								<li>
									<a href="pkm_dosen/pkm_dosen.php">Dosen PKM</a>
								</li>
								<li>
									<a href="master_dosen/master_dosen.php">Master Dosen</a>
								</li>
								<li>
									<a href="bimbingan_akademik/bimbingan_akademik.php">Bimbingan Akademik</a>
								</li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-right" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div> 
						<button type="submit" class="btn btn-default">
							Submit
						</button>
					</form>
				</div>
				
			</nav>
		</div>
	</div>
